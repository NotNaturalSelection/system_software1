class DiningPhilosophers {

    ReentrantLock[] forks = new ReentrantLock[5];
    public DiningPhilosophers() {
        for (int i = 0; i < forks.length; i++) {
            forks[i] = new ReentrantLock();
        }
    }

    // call the run() method of any runnable to execute its code
    public void wantsToEat(int philosopher,
                           Runnable pickLeftFork,
                           Runnable pickRightFork,
                           Runnable eat,
                           Runnable putLeftFork,
                           Runnable putRightFork) throws InterruptedException {
        int rightFork = philosopher;
        int leftFork = (philosopher + 1) % 5;

        ReentrantLock first = philosopher == 4 ? forks[rightFork] : forks[leftFork];
        if (philosopher == 4) {
            forks[4].lock();
            pickRightFork.run();
            forks[0].lock();
            pickLeftFork.run();

            eat.run();

            putLeftFork.run();
            forks[0].unlock();
            putRightFork.run();
            forks[4].unlock();
        } else {
            forks[philosopher + 1].lock();
            pickLeftFork.run();

            forks[philosopher].lock();
            pickRightFork.run();

            eat.run();
            putLeftFork.run();
            forks[philosopher + 1].unlock();
            putRightFork.run();
            forks[philosopher].unlock();
        }
    }
}
